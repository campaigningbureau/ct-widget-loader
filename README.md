# ct-widget-loader #

Load widgets from CBTools

## Changelog ##

### v1.0.3
 - The deprecated routes that require `customer_identifier` are no longer used, instead the new domain routes are always used. the `customer_identifier` is no longer required and was removed from the config

### v1.0.2
 - Basic Lumen Support (`config_path()` is no longer used, instead `path.config` is resolved from the IoC container)
 
### v1.0.1
 - `CTWidget::get` now supports additional http query parameters
