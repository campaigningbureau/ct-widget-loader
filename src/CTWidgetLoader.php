<?php

namespace Campaigningbureau\CTWidget;

use Carbon\Carbon;

/**
 * load a widget from the api or from the cache.
 *
 *
 * @package PrixCourage\Http\Controllers
 * @author Stefan Schindler <stefan.schindler@campaigning-bureau.com>
 */
class CTWidgetLoader
{
    /**
     * load the specified widget.
     *
     * @param string $widget_reference_identifier
     * @param bool $use_locale  defines if the current locale should be passed as lang parameter when retrieving the widget.
     * @return mixed
     */
    public function get($widget_reference_identifier, $use_locale = false, $additional_parameters = array())
    {
        $cacheKey = __CLASS__ . $widget_reference_identifier . ($use_locale ? \App::getLocale() : '.') . serialize($additional_parameters);

        // if the widget doesn't exist in the cache OR if we are in debug mode, load it directly from the api
        if(!\Cache::has($cacheKey) || \Config::get('app.debug')) {
            try {
                $widget = $this->loadWidgetFromAPI($widget_reference_identifier, $use_locale, $additional_parameters);
            }catch(\Exception $e)
            {
                return $e->getMessage();
            }

            \Cache::put($cacheKey, $widget, Carbon::now()->addHour());
        } else {
            $widget = \Cache::get($cacheKey);
        }

        return $widget;
    }

    /**
     * try to load the specified widget from the api.
     *
     * @param string    $widget_reference_identifier
     * @param bool  $use_locale  defines if the current locale should be passed as lang parameter when retrieving the widget.
     * @throws \Exception
     * @return mixed
     */
    private function loadWidgetFromAPI($widget_reference_identifier, $use_locale, $additional_parameters)
    {
        // check for mandatory config parmeters
        $this->checkConfigParameter('tools.host');

        if (!function_exists('curl_init')){
            die('Sorry cURL is not installed!');
        }

        $url = \Config::get('tools.host') . '/widgets/' . $widget_reference_identifier;

        $http_query = $additional_parameters;

        if($use_locale)
            $http_query['lang'] = \App::getLocale();

        $url .= '?' . http_build_query($http_query);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        // Download the given URL, and return output
        $output = curl_exec($ch);

        // Close the cURL resource, and free system resources
        curl_close($ch);

        return $output;
    }

    /**
     * check if a given config param exists.
     * if it doesn't exist, an log entry is performed an exception is thrown.
     *
     * @param string    $param
     * @throws \Exception
     */
    private function checkConfigParameter($param)
    {
        if(!\Config::has($param))
        {
            \Log::error('mandatory config parameter \'' . $param . '\' not found');

            if(\Config::get('app.debug'))
                throw new \Exception('mandatory config parameter \'' . $param . '\' not found');
            else
                throw new \Exception('could not load widget. check the log file for details');
        }
    }
}
