<?php
namespace Campaigningbureau\CTWidget;

use Illuminate\Support\ServiceProvider;

/**
 * acts as a service provider to load widgets.
 * registers a ct_widget singleton in the service container.
 *
 * @package Campaigningbureau\CTWidget
 * @author Stefan Schindler <stefan.schindler@campaigning-bureau.com>
 */
class CTWidgetServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {

        $this->publishes(array(
            __DIR__.'/config/config.php' => $this->app->make('path.config') . '/tools.php'
        ));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // merge default config with the current tools config.
        // all values that are set in the default package config (config.php), but not in the tools config, are written into the tools config,
        // so parameters needed by this package are definitely available.
        $this->mergeConfigFrom(
            __DIR__ . '/config/config.php',
            'tools'
        );

        $this->app->singleton('ct_widget', function()
        {
            return new CTWidgetLoader();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['ct_widget'];
    }
}
