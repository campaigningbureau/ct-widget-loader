<?php

namespace Campaigningbureau\CTWidget\Facades;

use Illuminate\Support\Facades\Facade;

class CTWidget extends Facade{

    protected static function getFacadeAccessor() { return 'ct_widget'; }

}
